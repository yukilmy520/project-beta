from django.db import models
from django.urls import reverse




class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    color = models.CharField(max_length=50, null=True)
    year = models.PositiveSmallIntegerField(null=True)
    is_sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return reverse("api_auto_vo", kwargs={"pk": self.id})




class Salesperson(models.Model):
    name = models.CharField(max_length=200)
    employee_num = models.SmallIntegerField()

    def __str__(self):
        return self.name
    def get_api_url(self):
        return reverse("api_salesperson", kwargs={"pk": self.id})


class Potential_Customer(models.Model):
    customer_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)

    def __str__(self):
        return self.customer_name
    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})


class SaleRecord(models.Model):
    price = models.SmallIntegerField()
    customer = models.ForeignKey(
        Potential_Customer,
        related_name= "sale_record",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sale_record",
        on_delete=models.CASCADE,
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sale_record",
        on_delete=models.CASCADE
    )

    def __str__(self):
        superthing = self.automobile.vin + ' | ' + self.customer.customer_name
        return superthing
