from django.urls import path
from .views import api_salespeople, api_customers, api_sale_records, api_show_automobile_vos, api_salesperson, api_customer

urlpatterns = [
    path('salespeople/', api_salespeople, name="list_salespeople"),
    path("customers/", api_customers, name="list_customers"),
    path("sale_records/", api_sale_records, name="list_sale_records"),
    path("autos/", api_show_automobile_vos, name="list_autos_vos"),
    path('salespeople/<int:pk>/', api_salesperson, name="show_salesperson"),
    path('customers/<int:pk>/', api_customer, name="show_customer"),
]
