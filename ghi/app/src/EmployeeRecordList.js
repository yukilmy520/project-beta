import React from 'react';




  class SalespersonRecordList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            salesPersons: [],
            salesPerson: '',
            salesRecords: [],
            allSalesRecords: [],
        };
        this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
    }

    handleSalesPersonChange(event) {
        const value = event.target.value;
        this.setState({ salesPerson: value })

        let resultRecords = []
        for (let record of this.state.allSalesRecords) {
            if (record["salesperson"]["name"] == value) {
                resultRecords.push(record)
            }
        }
        this.setState({ salesRecords: resultRecords })
    }

    async componentDidMount() {
        // get list of Sales Records
        const salesRecordsUrl = 'http://localhost:8090/api/sale_records/'
        const salesRecordsResponse = await fetch(salesRecordsUrl)

        if (salesRecordsResponse.ok) {
            const data = await salesRecordsResponse.json();
            const allSalesRecords = data.sale_records
            this.setState({ allSalesRecords: allSalesRecords })
        }

        // get list of Sales Persons
        const salesPersonsUrl = 'http://localhost:8090/api/salespeople/'
        const salesPersonsResponse = await fetch(salesPersonsUrl)

        if (salesPersonsResponse.ok) {
            const data = await salesPersonsResponse.json();
            let salesPersons = []
            for (let salesperson of data.salespeople) {
                salesPersons.push(salesperson)
            }
            this.setState({ salesPersons: salesPersons })
        }
    }

    render() {
        return (
            <>
                <div className='container'>
                    <h1>Sales Person History</h1>
                    <div className='mb-3'>
                        <select onChange={this.handleSalesPersonChange} value={this.state.salesPerson} required id='salesPerson' name='salesPerson' className='form-select'>
                            <option value=''>Choose a Sales Person</option>
                            {this.state.salesPersons && this.state.salesPersons.map(salesPerson => {
                                return (
                                    <option key={salesPerson.id} value={salesPerson.name}>{salesPerson.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <div className='mb-3'>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>Sales Person</th>
                                    <th>Customer</th>
                                    <th>VIN</th>
                                    <th>Sales Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.salesRecords && this.state.salesRecords.map(record => {
                                    return (
                                        <tr key={record["salesperson"]["id"]}>
                                            <td>{record["salesperson"]["name"]}</td>
                                            <td>{record["customer"]["customer_name"]}</td>
                                            <td>{record["automobile"]["vin"]}</td>
                                            <td>{record["price"]}</td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </>
        )
    }
}

  export default SalespersonRecordList;
