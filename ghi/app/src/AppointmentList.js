import { useEffect, useState } from "react";


const AppointmentList = () => {
    const [appointments, setAppointments] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8080/api/appointments/')
            .then(response => response.json())
            .then(data => {
                const data1 = data.appointments.filter(a=> (a.is_finished) === false)
                setAppointments(data1);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    const onDeleteAppointmentClick = (appointment) => {
        const appointmentHref = appointment.href;
        const hrefAppointments = appointment.href.split('/');
        const id = hrefAppointments[hrefAppointments.length-2];
        const AppointmentsUrl = `http://localhost:8080/api/appointments/${id}/`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(AppointmentsUrl, fetchConfig)
            .then(response => response.json())
            .then(data => {
                if (data.deleted) {
                    const currentAppointments = [...appointments];
                    setAppointments(currentAppointments.filter(appointment => appointment.href!== appointmentHref));
                }
            })
            .catch(e => console.log('error: ', e));
    }

    const onPutAppointmentClick = (appointment) => {
        const appointmentHref = appointment.href;
        const hrefAppointments = appointment.href.split('/');
        const id = hrefAppointments[hrefAppointments.length-2];
        const Appointments1Url = `http://localhost:8080/api/appointments/${id}/`;
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(
                {
                 is_finished:true
                }
            ),
            headers: {
                'Content-Type': 'application/json',
                },
        };
        fetch(Appointments1Url, fetchConfig)
            .then(response => response.json())
            .then(setAppointments(appointments.filter(appointment => appointment.href!== appointmentHref)))
    }

    return (
      <>
        <br/>
        <h1>Service Appointments</h1>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Vin</th>
                <th>Customer Name</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Vip</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {appointments && appointments.map(appointment => {
                return (
                <tr key={appointment.id}>
                    <td>{appointment.vin }</td>
                    <td>{appointment.customer_name }</td>
                    <td>{appointment.date }</td>
                    <td>{appointment.time }</td>
                    <td>{appointment.technician.technician_name }</td>
                    <td>{appointment.reason }</td>
                    <td>{String(appointment.vip)}</td>
                    <td>
                        <div className="btn-group">
                            <button onClick={() => onDeleteAppointmentClick(appointment)} type="button" className="btn btn-danger">Cancel</button>
                            <button onClick={() => onPutAppointmentClick(appointment)}  type="button" className="btn btn-success">Finished</button>
                        </div>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
      </>
    );
  }

  export default AppointmentList;
