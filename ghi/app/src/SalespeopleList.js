import { useEffect, useState } from "react";



const SalespeopleList = () => {
    const [salespeople, setSalespeople] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8090/api/salespeople/')
            .then(response => response.json())
            .then(data => {
                setSalespeople(data.salespeople);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    return (
      <>
        <h1>Salespeople</h1>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Employee Number</th>
            </tr>
            </thead>
            <tbody>
            {salespeople && salespeople.map(salesperson => {
                return (
                <tr key={salesperson.id}>
                    <td>{salesperson.name }</td>
                    <td>{salesperson.employee_num}</td>
                </tr>
                );
            })}
            </tbody>
        </table>
        <button onClick={() => window.location.href="http://localhost:3000/salespeople/new"}>Add a Salesperson</button>
      </>
    );
  }

  export default SalespeopleList;
