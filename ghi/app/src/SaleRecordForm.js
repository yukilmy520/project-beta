import React from 'react';
import { Link } from "react-router-dom";

class SaleRecordForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        price: '',
        automobile: '',
        autos: [],
        customer:'',
        customers: [],
        salesperson:'',
        salespeople: [],


    };

    this.handlePriceChange = this.handlePriceChange.bind(this);
    this.handleAutoChange = this.handleAutoChange.bind(this);
    this.handleCustomerChange = this.handleCustomerChange.bind(this);
    this.handleSalespersonChange = this.handleSalespersonChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.autos;
    delete data.customers;
    delete data.salespeople;


    const recordUrl = 'http://localhost:8090/api/sale_records/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(recordUrl, fetchConfig);
    if (response.ok) {
      const autoUrl = `http://localhost:8100${data.automobile}`
      const soldfetchConfig = {
        method: "put",
        body: JSON.stringify({is_sold: true}),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const SellResponse = await fetch(autoUrl, soldfetchConfig);

      const cleared = {
        price: '',
        automobile: '',
        customer:'',
        salesperson:''
      };
      this.setState(cleared);
      window.location.href=`http://localhost:3000/records/`
    }
  }

  handlePriceChange(event) {
    const value = event.target.value;
    this.setState({price: value})
  }

  handleAutoChange(event) {
    const value = event.target.value;
    this.setState({automobile: value})
  }

  handleCustomerChange(event) {
    const value = event.target.value;
    this.setState({customer: value})
  }

  handleSalespersonChange(event) {
    const value = event.target.value;
    this.setState({salesperson: value})
  }

  async componentDidMount() {
    const urlA = `http://localhost:8090/api/autos/`;
    const urlC = `http://localhost:8090/api/customers/`;
    const urlS = `http://localhost:8090/api/salespeople/`;

    const responseAuto = await fetch(urlA);
    const responseCust = await fetch(urlC);
    const responseSales = await fetch(urlS);

    if (responseAuto.ok && responseCust.ok && responseSales.ok) {
      const dataAutos = await responseAuto.json();
      const dataCust = await responseCust.json();
      const dataSales = await responseSales.json();
    const filteredAutos = dataAutos.autos.filter( auto => auto.is_sold === false);
      this.setState({autos: filteredAutos});
      this.setState({customers: dataCust.customers});
      this.setState({salespeople: dataSales.salespeople});
      //console.log(data)
    }
  }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create New Sale Record</h1>
            <form onSubmit={this.handleSubmit} id="create-sale_record-form">
              <div className="form-floating mb-3">
                <input value={this.state.color} onChange={this.handlePriceChange} placeholder="price" required type="text" name="price" id="price" className="form-control" />
                <label htmlFor="price">Price</label>
              </div>
              <div className="mb-3">
                <select value={this.state.automobile} onChange={this.handleAutoChange } required name="automobile" id="automobile" className="form-select">
                  <option value="">Choose an Automobile</option>
                  {this.state.autos && this.state.autos.map(automobile => {
                    return (
                      <option key={automobile.href} value={automobile.href}>
                        {automobile.vin}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select value={this.state.customer} onChange={this.handleCustomerChange } required name="customer" id="customer" className="form-select">
                  <option value="">Select a Valued Customer</option>
                  {this.state.customers && this.state.customers.map(customer => {
                    return (
                      <option key={customer.id} value={customer.id}>
                        {customer.customer_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select value={this.state.salesperson} onChange={this.handleSalespersonChange } required name="salesperson" id="salesperson" className="form-select">
                  <option value="">Choose a Salesperson</option>
                  {this.state.salespeople && this.state.salespeople.map(salesperson => {
                    return (
                      <option key={salesperson.id} value={salesperson.id}>
                        {salesperson.name}
                      </option>
                    );
                  })}
                </select>
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
            <Link to={"/records"}>Return to Records</Link>
          </div>
        </div>
      </div>
    );
  }
}

export default SaleRecordForm;
