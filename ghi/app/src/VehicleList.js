import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const VehicleList = () => {
    const [models, setVehicle] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8100/api/models/')
            .then(response => response.json())
            .then(data => {
                setVehicle(data.models);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    return (
        <>
            <br/>
            <h1>Vehicle Models</h1>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
                </thead>
                <tbody>
                {models && models.map(model=> {
                    return (
                    <tr key={model.href}>
                        <td width="16.66%">{ model.name }</td>
                        <td width="16.66%">{ model.manufacturer.name }</td>
                        <td width="16.66%"><img src={model.picture_url} alt=''className="img-rounded" width = "50%" height="50%" /></td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
            <Link to={"/vehicles/new"}>Add Vehicles</Link>
        </>
    );
  }

  export default VehicleList;
