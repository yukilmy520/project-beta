import { useEffect, useState } from "react";


const TechnicianList = () => {
    const [technicians, setTechnicians] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8080/api/technicians/')
            .then(response => response.json())
            .then(data => {
                setTechnicians(data.technicians);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    return (
      <>
        <br/>
        <h1>Technicians List</h1>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Employee Number</th>
            </tr>
            </thead>
            <tbody>
            {technicians && technicians.map(technician => {
                return (
                <tr key={technician.id}>
                    <td>{technician.technician_name }</td>
                    <td>{technician.employee_number }</td>
                </tr>
                );
            })}
            </tbody>
        </table>
      </>
    );
  }

  export default TechnicianList;
