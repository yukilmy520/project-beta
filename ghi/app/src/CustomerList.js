import { useEffect, useState } from "react";



const CustomerList = () => {
    const [customers, setCustomers] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8090/api/customers/')
            .then(response => response.json())
            .then(data => {
                setCustomers(data.customers);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    return (
      <>
        <h1>Our Valued Customers</h1>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Phone Number</th>
                <th>Address</th>
            </tr>
            </thead>
            <tbody>
            {customers && customers.map(customer => {
                return (
                <tr key={customer.id}>
                    <td>{customer.customer_name }</td>
                    <td>{customer.phone_number}</td>
                    <td>{customer.address}</td>
                </tr>
                );
            })}
            </tbody>
        </table>
        <button onClick={() => window.location.href="http://localhost:3000/customers/new"}>Add a New Customer</button>
      </>
    );
  }

  export default CustomerList;
