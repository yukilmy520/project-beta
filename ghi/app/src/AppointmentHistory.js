import { useEffect, useState } from "react";


const AppointmentHistoryList = () => {

    const [appointments, setAppointments] = useState([]);
    const [appointments1, setAppointments1] =useState([]);
    const [search, setSearch] = useState("");

    useEffect(() => {
        fetch('http://localhost:8080/api/appointments/')
            .then(response => response.json())
            .then(data => {
                setAppointments(data.appointments);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    const onClickAppointmentClick = () => {
        if(search){
        const currentAppointments = [...appointments];
        const currentAppointments1= currentAppointments.filter(appointment => (appointment.is_finished) === true);
        setAppointments1(currentAppointments1.filter(appointment => {return appointment.vin.includes(search)}));
        }
    }

    return (
      <>
        <br/>
        {/* <input onChange = {(e)=>{setSearch(e.target.value)}} type="text" placeholder="Search.."></input> */}
        <div className="input-group mb-3">
            <input onChange = {(e)=>{setSearch(e.target.value)}} type="text"  className="form-control" placeholder="VIN.." aria-describedby="basic-addon1"></input>
            <button onClick={() => onClickAppointmentClick()} type="button" className="btn-group btn-group-sm" role="group" aria-label="...">Search VIN</button>
            </div>
        <hr/>
        <h1>Service Appointments</h1>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Vin</th>
                <th>Customer Name</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Vip</th>
            </tr>
            </thead>
            <tbody>
            {appointments1 && appointments1.map(appointment => {
                return (
                <tr key={appointment.id}>
                    <td>{appointment.vin }</td>
                    <td>{appointment.customer_name }</td>
                    <td>{appointment.date }</td>
                    <td>{appointment.time }</td>
                    <td>{appointment.technician.technician_name }</td>
                    <td>{appointment.reason }</td>
                    <td>{String(appointment.vip)}</td>
                </tr>
                );
            })}
            </tbody>
        </table>
      </>
    );
  }

  export default AppointmentHistoryList;
