import React from 'react';

class VehicleForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      picture_url: '',
      manufacturer_id:'',
      manufacturers: []
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handlePictureChange = this.handlePictureChange.bind(this);
    this.handleManufacturersChange = this.handleManufacturersChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.manufacturers;


    const VehicleUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(VehicleUrl, fetchConfig);
    if (response.ok) {
      const newVehicle = await response.json();

      const cleared = {
        name: '',
        picture_url: '',
        manufacturer_id:'',
      };
      this.setState(cleared);
      window.location.href='http://localhost:3000/vehicles'
    }
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({name: value})
  }

  handlePictureChange(event) {
    const value = event.target.value;
    this.setState({picture_url: value})
  }

  handleManufacturersChange(event) {
    const value = event.target.value;
    this.setState({manufacturer_id: value})
  }


  async componentDidMount() {
    const url = `http://localhost:8100/api/manufacturers/`;

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({manufacturers: data.manufacturers});
      //console.log(data)
    }
  }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a vehicle model</h1>
            <form onSubmit={this.handleSubmit} id="create-shoes-form">
              <div className="form-floating mb-3">
                <input value={this.state.name} onChange={this.handleNameChange} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.picture_url} onChange={this.handlePictureChange} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture Url</label>
              </div>
              <div className="mb-3">
                <select value={this.state.manufacturer} onChange={this.handleManufacturersChange} required name="manufacturer" id="manufacturer" className="form-select">
                  <option value="">Choose a manufacturer</option>
                  {this.state.manufacturers&&this.state.manufacturers.map(manufacturer => {
                    return (
                      <option key={manufacturer.id} value={manufacturer.id}>
                        {manufacturer.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default VehicleForm;
